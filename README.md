# Expense Reimbursement System

## Project Description

The Expense Reimbursement System (ERS) manages the process of reimbursing 
employees for expenses incurred while on company time. All employees in the company 
can login and submit requests for reimbursement and view their past tickets and pending 
requests. Finance managers can log in and view all reimbursement requests and past 
history for all employees in the company. Finance managers are authorized to approve 
and deny requests for expense reimbursement.

## Technologies Used

* Javalin - version 3.10.1
* Hibernate - version 5.4.21
* Mockito - version 1.10.19
* JUnit - version 4.12
* Selenium - version 3.141.59

## Features

List of features ready and TODOs for future development
* Employee can submit a reimbursement for approval.
* Employee can review all previously submitted reimbursements.
* Finance Manager can view all submitted reimbursements and filter based on status.
* Finance Manager can approve or deny pending requests.

To-do list:
* Implement sign-up for users
* Encrypt password information

## Getting Started
   
- Open git and clone using `git clone https://gitlab.com/johnpjacobelli/expense-reimbursement-system.git`
- Have a Java IDE, such as Eclipse or Spring downloaded.

## Usage

- Open perferred IDE
- Import existing project
- Locate folder from git


![ERS folder](https://i.imgur.com/3nnLRZT.png)
- Open project
- Run MainDriver.java as Java Application


![Driver](https://i.imgur.com/qpIAObZ.png)
- Navigate to `http://localhost:9001/html/index.html`
- The index below should display, and is ready to be used!


![Main page](https://i.imgur.com/NhtTf13.png)
